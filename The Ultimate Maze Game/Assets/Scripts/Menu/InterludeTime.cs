﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InterludeTime : MonoBehaviour
{
    [SerializeField] private string stageNameSpace = "Easy ";

    [SerializeField] private GameTimer gameTimer;
    [SerializeField] private Text yourTimeText;
    [SerializeField] private Text bestTimeText;

    private void Start()
    {
        var span = TimeSpan.FromSeconds(GameTimer.SumOfTimes);
        yourTimeText.text = "Your Time:\n<color='yellow'>" + gameTimer.FormatTimeHours(GameTimer.SumOfTimes) + "</color>";

        if (Util.StartedAtStage != 0)
        {
            yourTimeText.text = "Start from stage 1 to\nget a valid time";
        }

        var bestTime = 0f;
        for (int i = 0; i < 10; i++)
        {
            var time = SaveSystem.GetFloat(stageNameSpace + (i + 1), 0f);
            bestTime += time;
        }

        span = TimeSpan.FromSeconds(bestTime);
        bestTimeText.text = "Best Time:\n<color='cyan'>" + gameTimer.FormatTimeHours(bestTime) + "</color>";

        var storedTime = SaveSystem.GetFloat("Best Time", 9999999f);
        if (bestTime < storedTime)
        {
            SaveSystem.SetFloat("Best Time", bestTime);
        }

        Util.StartedAtStage = 0;
        // string.Format(@"{0:hh\:mm\:ss\.ff}"
    }
}
