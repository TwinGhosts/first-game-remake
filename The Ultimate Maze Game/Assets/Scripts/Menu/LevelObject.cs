﻿using UnityEngine;
using UnityEngine.UI;

public class LevelObject : MonoBehaviour
{
    private void Awake()
    {
        var text = GetComponentInChildren<Text>();
        int.TryParse(name, out var index);
        text.text = index.ToString();

        if (index != 1)
        {
            var button = GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                Util.StartedAtStage = 1;
                Debug.Log("Not started at stage 1");
            });
        }
    }
}
