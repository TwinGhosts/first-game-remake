﻿using UnityEngine;

public class MenuManager : MonoBehaviour
{
    private void Start()
    {
        SaveSystem.Initialize("Save");
    }

    public void UnPause()
    {
        Time.timeScale = 1f;
    }

    public void PlayOldGame()
    {
        // Application.OpenURL("https://mega.nz/#!HNFB2ACb!RXdJ4PYMBFtGq__HExEY3QZr4nxs7s444qaRtvJBLhk");
    }

    public void Link()
    {
        Application.OpenURL("https://twinghosts.itch.io/");
    }

    public void Quit()
    {
        Util.FadeManager.Quit();
    }

    public void RestartGame(string scene)
    {
        GameTimer.SumOfTimes = 0;
        Util.StartedAtStage = 0;
        Util.FadeManager.GoToScene(scene);
    }

    public void Play()
    {
        GameTimer.SumOfTimes = 0;
        Util.StartedAtStage = 0;
        Util.FadeManager.GoToScene("Difficulty Select");
    }

    public void LevelSelect()
    {
        GameTimer.SumOfTimes = 0;
        Util.StartedAtStage = 0;
        Util.FadeManager.GoToScene("Level Select");
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        Util.StartedAtStage = 0;
        GameTimer.SumOfTimes = 0;
        Util.FadeManager.GoToScene("Main Menu");
    }

    public void EraseData(int tag)
    {
        switch ((EraseTag)tag)
        {
            case EraseTag.Easy:
                for (int i = 0; i < 10; i++)
                {
                    SaveSystem.SetFloat("Easy " + i, 999999f);
                }
                break;
            case EraseTag.Medium:
                for (int i = 0; i < 10; i++)
                {
                    SaveSystem.SetFloat("Medium " + i, 999999f);
                }
                break;
            case EraseTag.Hard:
                for (int i = 0; i < 10; i++)
                {
                    SaveSystem.SetFloat("Hard " + i, 999999f);
                }
                break;
            case EraseTag.All:
                for (int i = 0; i < 10; i++)
                {
                    SaveSystem.SetFloat("Easy " + i, 999999f);
                    SaveSystem.SetFloat("Medium " + i, 999999f);
                    SaveSystem.SetFloat("Hard " + i, 999999f);
                    SaveSystem.SetFloat("Best Time", 999999f);
                }
                break;
        }
    }
}

public enum EraseTag
{
    Easy,
    Medium,
    Hard,
    All,
}