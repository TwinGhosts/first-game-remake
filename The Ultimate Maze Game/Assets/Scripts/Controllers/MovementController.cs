using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private float speed = 1f;

    private Rigidbody2D body;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    public void Move(float horizontalInput, float verticalInput)
    {
        var input = new Vector2(horizontalInput, verticalInput);
        if (input != Vector2.zero)
        {
            body.AddForce(input.normalized * speed, ForceMode2D.Force);
        }
    }
}