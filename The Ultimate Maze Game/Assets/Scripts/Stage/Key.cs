﻿using System.Collections;
using UnityEngine;

public class Key : MonoBehaviour
{
    [SerializeField] private KeyType type;

    public KeyType Type => type;

    private Gate targetGate;
    private bool isUnlocking = false;

    private Vector2 initialPosition;

    private const float unlockMoveDuration = 0.5f;

    public void Initialize(Gate targetGate)
    {
        if (targetGate == null)
        {
            Debug.LogWarning($"No gate found for key with type: '{type}'");
            Destroy(gameObject);
        }

        this.targetGate = targetGate;

        initialPosition = transform.position;
    }

    public void Reset()
    {
        isUnlocking = false;
        transform.position = initialPosition;

        StopCoroutine(_Unlock());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!isUnlocking && other.CompareTag(Tags.Player))
        {
            isUnlocking = true;
            StartCoroutine(_Unlock());
        }
    }

    private IEnumerator _Unlock()
    {
        var progress = 0f;
        var startPosition = transform.position;
        var endPosition = targetGate.transform.position;

        while (progress < 1f)
        {
            progress += Time.deltaTime / unlockMoveDuration;
            transform.position = Vector2.Lerp(startPosition, endPosition, progress);
            yield return null;
        }

        targetGate.Open();
        transform.position = Vector2.one * 9999f;
    }
}
