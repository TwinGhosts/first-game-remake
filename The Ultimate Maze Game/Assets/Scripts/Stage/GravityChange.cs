﻿using UnityEngine;

public class GravityChange : MonoBehaviour
{
    [SerializeField] private Vector2 gravity;

    public void Activate()
    {
        Physics2D.gravity = gravity;
        Util.Player.SetGravityScale(1f);
    }

    private void Awake()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, MathEx.AngleFromVector(gravity) - 90f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.Player))
        {
            Activate();
        }
    }
}
