﻿using UnityEngine;

public class PushBlock : MonoBehaviour
{
    [SerializeField] private bool isActive = true;

    [SerializeField] private Sprite activeSprite;
    [SerializeField] private Sprite inactiveSprite;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D body;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        body = GetComponent<Rigidbody2D>();

        Switch(isActive);
    }

    public void Toggle()
    {
        isActive = !isActive;
        spriteRenderer.sprite = (isActive) ? activeSprite : inactiveSprite;
        body.isKinematic = !isActive;
    }

    public void Switch(bool state)
    {
        isActive = state;
        spriteRenderer.sprite = (isActive) ? activeSprite : inactiveSprite;
        body.isKinematic = !isActive;
    }
}
