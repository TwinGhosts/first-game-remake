﻿using UnityEngine;

public class GravityResetter : MonoBehaviour
{
    public void Activate()
    {
        Physics2D.gravity = Vector2.down;
        Util.Player.SetGravityScale(0f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.Player))
        {
            Activate();
        }
    }
}
