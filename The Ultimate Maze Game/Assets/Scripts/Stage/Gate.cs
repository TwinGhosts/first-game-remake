﻿using System.Collections;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private KeyType type;
    [SerializeField] private bool isOpen = false;

    public bool IsOpen => isOpen;
    public bool IsOpening => isOpening;
    public KeyType Type => type;

    private bool isOpening = false;
    private new Collider2D collider;
    private SpriteRenderer spriteRenderer;

    private readonly float openDuration = 0.5f;

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Reset()
    {
        StopCoroutine(_Open());

        isOpen = false;
        isOpening = false;
        collider.enabled = true;
        spriteRenderer.color = Color.white;
    }

    public void Open()
    {
        if (!isOpen && !isOpening)
            StartCoroutine(_Open());
    }

    private IEnumerator _Open()
    {
        var progress = 0f;
        var startColor = spriteRenderer.color;
        var endColor = startColor;
        endColor.a = 0f;

        isOpening = true;

        while (progress < 1f)
        {
            progress += Time.deltaTime / openDuration;
            spriteRenderer.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        collider.enabled = false;
        isOpen = true;
        isOpening = false;
    }
}
