﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    private bool endReached = false;
    private GameTimer gameTimer;
    private FadeManager fadeManager;

    private void Start()
    {
        gameTimer = FindObjectOfType<GameTimer>();
        fadeManager = FindObjectOfType<FadeManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player) && !endReached)
        {
            endReached = true;

            gameTimer.EndStage();
            fadeManager.GoToScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        if (collision.CompareTag(Tags.AI))
        {
            var actor = collision.GetComponent<AINeural>();
            if (actor.IsAlive)
            {
                actor.Finish(gameTimer.ElapsedTime);
            }
        }
    }
}
