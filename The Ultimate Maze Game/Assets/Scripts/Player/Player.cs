﻿using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private ParticleSystem deathSystem;

    private bool isAlive = true;
    private Rigidbody2D body;
    private SpriteRenderer spriteRenderer;
    private new Collider2D collider;
    private MovementController movementController;

    private float restartTimer = 1f;

    private void Awake()
    {
        Util.Player = this;

        body = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        movementController = GetComponent<MovementController>();
    }

    private void Update()
    {
        if (!isAlive)
        {
            restartTimer -= Time.deltaTime;
            if (restartTimer <= 0f)
            {
                restartTimer = 500f;
                Util.FadeManager.RestartScene();
            }
        }
    }

    private void FixedUpdate()
    {
        Movement();
    }

    public void SetGravityScale(float scale)
    {
        body.gravityScale = scale;
    }

    public void Death()
    {
        collider.enabled = false;
        ProCamera2DShake.Instance.Shake(0.3f, Vector2.one * 4);
        body.velocity = Vector2.zero;
        body.simulated = false;
        deathSystem.Play();
        spriteRenderer.enabled = false;
        isAlive = false;
    }

    private void Movement()
    {
        if (isAlive)
        {
            movementController.Move(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isAlive) return;

        var hostile = collision.collider.CompareTag(Tags.Hostile);
        if (hostile)
        {
            Death();
        }
    }
}
