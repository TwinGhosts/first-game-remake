using UnityEngine;

public static class ColorGenerator
{
    public static Color GeneratePastelColor()
    {
        var hue = Random.Range(0f, 1f);
        var saturation = Random.Range(0.4f, 0.6f);
        var value = Random.Range(0.8f, 1f);

        var color = Color.HSVToRGB(hue, saturation, value);
        color.a = 1f;

        return color;
    }
}