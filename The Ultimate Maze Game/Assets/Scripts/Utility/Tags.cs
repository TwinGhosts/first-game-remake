public static class Tags
{
    public const string Hostile = "Hostile";
    public const string Player = "Player";
    public const string AI = "AI";
}