﻿using Sirenix.OdinInspector;
using UnityEngine;

[ExecuteInEditMode]
public class EditorSnap : MonoBehaviour
{
    private void Start()
    {
        SnapToGrid();
    }

    [Button("Snap")]
    public void SnapToGrid()
    {
        transform.position = new Vector2(Mathf.Floor(transform.position.x) + 0.5f, Mathf.Floor(transform.position.y) + 0.5f);
    }
}
