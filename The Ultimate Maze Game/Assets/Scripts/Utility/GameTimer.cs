﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private Text bestTimeText;
    [SerializeField] private Graphic pauseMenu;

    public static float SumOfTimes { get; set; }
    public float ElapsedTime => elapsedTime;
    public float BestTime => bestTime;

    private Color color;
    private float elapsedTime = 0;
    private float bestTime = 0;

    private bool stageActive = true;

    private void Start()
    {
        text.enabled = Util.TimerEnabled;
        bestTimeText.enabled = Util.TimerEnabled;

        color = bestTimeText.color;
        bestTime = SaveSystem.GetFloat(SceneManager.GetActiveScene().name, Mathf.Infinity);
        if (bestTime != Mathf.Infinity && bestTime != 999999f)
        {
            bestTimeText.text = FormatTimeNoHours(bestTime);
        }
        else
        {
            bestTimeText.text = "";
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = (Time.timeScale == 0f) ? 1f : 0f;
        }

        pauseMenu.gameObject.SetActive(Time.timeScale == 0f);

        if (stageActive)
            elapsedTime = Time.timeSinceLevelLoad;

        if (text.enabled)
            text.text = FormatTimeNoHours(elapsedTime);

        bestTimeText.color = (elapsedTime <= bestTime) ? color : Color.red;

        if (Input.GetKeyDown(KeyCode.T))
        {
            Util.TimerEnabled = !Util.TimerEnabled;
            text.enabled = Util.TimerEnabled;
            bestTimeText.enabled = Util.TimerEnabled;
        }
    }

    public void EndStage()
    {
        stageActive = false;
        SumOfTimes += elapsedTime;
        if (elapsedTime < bestTime)
        {
            bestTime = elapsedTime;
            bestTimeText.text = FormatTimeNoHours(bestTime);
            SaveSystem.SetFloat(SceneManager.GetActiveScene().name, bestTime);
        }
    }

    public string FormatTimeNoHours(float time)
    {
        var intTime = (int)time;
        var minutes = intTime / 60;
        var seconds = intTime % 60;
        var fraction = time * 1000;

        fraction %= 1000;

        var timeText = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, fraction);
        return timeText;
    }

    public string FormatTimeHours(float time)
    {
        var intTime = (int)time;
        var minutes = intTime / 60;
        var hours = Mathf.FloorToInt(minutes / 60f);
        var seconds = intTime % 60;
        var fraction = time * 1000;

        fraction %= 1000;

        var timeText = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", hours, minutes, seconds, fraction);
        return timeText;
    }
}
