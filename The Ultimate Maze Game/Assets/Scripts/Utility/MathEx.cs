﻿using UnityEngine;

public static class MathEx
{
    public static Vector3 VectorFromAngle(float dir)
    {
        return new Vector3(Mathf.Cos(Mathf.Deg2Rad * dir), Mathf.Sin(Mathf.Deg2Rad * dir));
    }

    public static float AngleFromVector(Vector2 dir)
    {
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public static float Clamp0360(float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result;
    }

    public static float ClampMinus180Positive180(float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result - 180f;
    }

    public static Vector2 QuadraticCurve(Vector2 a, Vector2 b, Vector2 c, float t)
    {
        var p0 = Vector2.Lerp(a, b, t);
        var p1 = Vector2.Lerp(a, c, t);
        return Vector2.Lerp(p0, p1, t);
    }

    public static Vector2 CubicCurve(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float t)
    {
        var p0 = QuadraticCurve(a, b, c, t);
        var p1 = QuadraticCurve(b, c, d, t);
        return Vector2.Lerp(p0, p1, t);
    }

    public static Vector2 EvaluateQuadratic(Vector2 a, Vector2 b, Vector2 c, float t)
    {
        Vector2 p0 = Vector2.Lerp(a, b, t);
        Vector2 p1 = Vector2.Lerp(b, c, t);
        return Vector2.Lerp(p0, p1, t);
    }

    public static Vector2 EvaluateCubic(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float t)
    {
        Vector2 p0 = EvaluateQuadratic(a, b, c, t);
        Vector2 p1 = EvaluateQuadratic(b, c, d, t);
        return Vector2.Lerp(p0, p1, t);
    }

}