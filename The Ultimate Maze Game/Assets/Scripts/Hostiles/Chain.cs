﻿using System.Collections.Generic;
using UnityEngine;

public class Chain : MonoBehaviour
{
    [Header("Basics")]
    [SerializeField] private int length = 5;
    [SerializeField] private float speed = 360f;
    [SerializeField] private float angle = 0f;
    [SerializeField] private TurnDirection turnDirection = TurnDirection.Right;

    [Header("References")]
    [SerializeField] private FireBall fireBallPrefab;

    private readonly List<FireBall> fireBalls = new();
    private const float fireBallSize = 0.33f;
    private EdgeCollider2D edgeCollider;
    private new ParticleSystem particleSystem;

    private void Awake()
    {
        edgeCollider = GetComponent<EdgeCollider2D>();
        particleSystem = GetComponent<ParticleSystem>();
        edgeCollider.points = new Vector2[] { Vector2.zero, MathEx.VectorFromAngle(angle).normalized * (length - 1) * fireBallSize };
        var psShape = particleSystem.shape;
        psShape.radius = Vector2.Distance(Vector2.zero, MathEx.VectorFromAngle(angle).normalized * (length - 1) * fireBallSize) / 2f;
    }

    // Use this for initialization
    private void Start()
    {
        for (int i = 0; i < length; i++)
        {
            var fireBall = Instantiate(fireBallPrefab, transform.position, Quaternion.identity);
            fireBalls.Add(fireBall);
            fireBall.transform.SetParent(gameObject.transform);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        var particleSystemShape = particleSystem.shape;
        angle = MathEx.Clamp0360(turnDirection.Equals(TurnDirection.Right) ? angle + Time.deltaTime * speed : angle - Time.deltaTime * speed);
        Vector2 pos = (length - 1) * fireBallSize * MathEx.VectorFromAngle(angle).normalized;
        var colliderpoints = edgeCollider.points;
        colliderpoints[1] = pos;
        edgeCollider.points = colliderpoints;

        particleSystemShape.position = (length - 1) * fireBallSize * MathEx.VectorFromAngle(angle).normalized * 0.5f;
        particleSystemShape.rotation = new Vector3(0f, 0f, angle);

        var index = 0;
        foreach (var fire in fireBalls)
        {
            fire.transform.position = transform.position + fireBallSize * index * MathEx.VectorFromAngle(angle).normalized;
            index++;
        }
    }

    public void Reset()
    {
        angle = 0f;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + fireBallSize * length * MathEx.VectorFromAngle(angle).normalized);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
            Util.Player.Death();
    }
}