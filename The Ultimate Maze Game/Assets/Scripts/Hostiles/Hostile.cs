﻿using UnityEngine;

public class Hostile : MonoBehaviour
{
    [Header("Base")]
    [SerializeField] protected bool rotateToDirection = true;
    [SerializeField] protected bool animationSpeedBasedOnVelocity = true;

    [Header("Stats")]
    [SerializeField] protected float speed = 100f;

    protected Rigidbody2D body;
    protected new CircleCollider2D collider;
    protected Vector2 directionVector;
    protected Animator animator;

    protected float initialSpeed;
    protected Quaternion initialRotation;
    protected Vector2 initialVelocity;
    protected Vector2 initialPosition;
    protected Vector2 initialDirectionVector;

    protected void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        collider = GetComponent<CircleCollider2D>();
        animator = GetComponent<Animator>();

        initialSpeed = speed;
        initialRotation = transform.rotation;
        initialVelocity = body.velocity;
        initialPosition = transform.position;
        initialDirectionVector = directionVector;
    }

    protected virtual void Update()
    {
        RotateToDirection();
    }

    public virtual void Reset()
    {
        transform.rotation = initialRotation;
        transform.position = initialPosition;
        body.velocity = initialVelocity;
        directionVector = initialDirectionVector;
        speed = initialSpeed;
    }

    protected void RotateToDirection()
    {
        if (!rotateToDirection) return;

        transform.rotation = Quaternion.Euler(0f, 0f, MathEx.AngleFromVector(body.velocity) - 90f);
    }

    protected void DynamicAnimationSpeed()
    {
        if (!animationSpeedBasedOnVelocity) return;

        animator.speed = 1 + (body.velocity.magnitude);
    }
}
