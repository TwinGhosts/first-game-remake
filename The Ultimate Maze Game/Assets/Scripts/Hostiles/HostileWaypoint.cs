﻿using UnityEngine;

public class HostileWaypoint : Hostile
{
    [Header("Custom")]
    [SerializeField] protected Transform[] waypoints = new Transform[] { };
    [SerializeField] protected bool reverseAtEnd = true;

    private bool reverse = false;
    private int index = 0;

    protected void Start()
    {
        transform.position = waypoints[0].position;
        index = 0;
    }

    protected override void Update()
    {
        var nextIndex = 0;
        var nextWaypoint = waypoints[0];

        if (reverseAtEnd)
        {
            nextIndex = index + ((reverse) ? -1 : 1);
            if (nextIndex == -1 || nextIndex == waypoints.Length)
            {
                reverse = !reverse;
                nextIndex = (reverse) ? waypoints.Length - 2 : 1;
            }
        }
        else
        {
            nextIndex = (int)Mathf.Repeat(index + 1, waypoints.Length);
        }

        nextWaypoint = waypoints[nextIndex];
        var direction = Vector2.MoveTowards(transform.position, nextWaypoint.position, speed * Time.deltaTime);
        transform.position = direction;

        if (Vector2.Distance(transform.position, nextWaypoint.position) < 0.001f * speed)
        {
            index = nextIndex;
        }

        transform.rotation = Quaternion.Euler(0f, 0f, MathEx.AngleFromVector(nextWaypoint.position - transform.position) - 90f);

        if (!animationSpeedBasedOnVelocity)
            return;

        animator.speed = 1 + (speed / 4);
    }

    public override void Reset()
    {
        base.Reset();

        transform.position = waypoints[0].position;
        index = 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = ColorGenerator.GeneratePastelColor();
        for (int i = 0; i < waypoints.Length; i++)
            Gizmos.DrawWireCube(waypoints[i].position, Vector3.one * 0.5f);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        var hostile = collider.CompareTag(Tags.AI);
        var player = collider.CompareTag(Tags.Player);

        if (hostile)
        {
            collider.GetComponent<AINeural>().Kill();
        }
        else if (player)
        {
            collider.GetComponent<Player>().Death();
        }
    }
}