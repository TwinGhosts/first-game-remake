﻿using UnityEngine;

public class FireBall : MonoBehaviour
{
    private const float rotationSpeed = 720f;

    private Vector2 initialPosition;

    private void Awake()
    {
        initialPosition = transform.position;
    }

    private void Update()
    {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }

    public void Reset()
    {
        transform.position = initialPosition;
    }
}