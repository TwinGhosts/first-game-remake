﻿using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    [SerializeField] private float fallDuration = 2f;
    [SerializeField] private Collider2D damageCollider;
    [SerializeField] private Collider2D blockingCollider;
    [SerializeField] private Collider2D generalCollider;

    private Animator animator;
    private bool isActive = false;
    private bool isFinished = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        damageCollider.enabled = false;
        generalCollider.enabled = true;
        blockingCollider.enabled = false;
        animator.speed = 0;
    }

    private void Activate()
    {
        isActive = true;
        animator.Play("Leaf Trap");
        animator.speed = 1f / fallDuration;
    }

    public void Finished()
    {
        isFinished = true;
        blockingCollider.enabled = true;
        generalCollider.enabled = false;
        damageCollider.enabled = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.CompareTag(Tags.Player);
        if (!isActive && player && !isFinished)
        {
            Activate();
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        var player = collision.CompareTag(Tags.Player);
        if (!isFinished && isActive && player)
        {
            ProCamera2DShake.Instance.Shake(0.05f, Vector2.one * 0.1f);
        }
        else if (isFinished && damageCollider.enabled && player)
        {
            Util.Player.Death();
        }
    }
}
