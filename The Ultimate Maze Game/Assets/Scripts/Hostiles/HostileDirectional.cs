﻿using UnityEngine;

public class HostileDirectional : Hostile
{
    [Header("Custom")]
    [SerializeField] protected Direction direction;

    private ParticleSystem trailSystem;

    private void Start()
    {
        trailSystem = GetComponent<ParticleSystem>();

        DetermineDirection();
    }

    protected override void Update()
    {
        base.Update();

        DynamicAnimationSpeed();
        var emission = trailSystem.emission;
        emission.rateOverTime = 8 + body.velocity.magnitude;
    }

    public override void Reset()
    {
        base.Reset();

        DetermineDirection();
    }

    private void DetermineDirection()
    {
        // Determine direction to move in
        switch (direction)
        {
            default:
            case Direction.Up:
                directionVector = Vector2.up;
                break;
            case Direction.Down:
                directionVector = Vector2.down;
                break;
            case Direction.Left:
                directionVector = Vector2.left;
                break;
            case Direction.Right:
                directionVector = Vector2.right;
                break;
            case Direction.UpRight:
                directionVector = Vector2.right + Vector2.up;
                break;
            case Direction.UpLeft:
                directionVector = Vector2.left + Vector2.up;
                break;
            case Direction.DownRight:
                directionVector = Vector2.right + Vector2.down;
                break;
            case Direction.DownLeft:
                directionVector = Vector2.left + Vector2.down;
                break;
        }

        body.AddForce(directionVector.normalized * speed);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        var hostile = collider.CompareTag(Tags.AI);
        var player = collider.CompareTag(Tags.Player);

        if (hostile)
        {
            collider.GetComponent<AINeural>().Kill();
        }
        else if (player)
        {
            collider.GetComponent<Player>().Death();
        }
    }
}