﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    [SerializeField] private Graphic _fadeGraphic = null;
    [SerializeField] private bool isFadedIn = false;

    public Action OnFadeOut = () => { };
    public Action OnFadeIn = () => { };

    private Color color;
    private bool isFading = false;

    private AsyncOperation loadOperation;

    private const float STANDARD_FADE_DURATION = 0.4f;

    private void Awake()
    {
        Util.FadeManager = this;

        _fadeGraphic.gameObject.SetActive(true);
    }

    // Use this for initialization
    private void Start()
    {
        color = _fadeGraphic.color;
        Fade(true);
    }

    private void Update()
    {
        // Test for controlling the loading
        if (loadOperation != null && loadOperation.progress >= 0.9f)
        {
            loadOperation.allowSceneActivation = true;
        }
    }

    private void Fade(bool fadeIn)
    {
        if (!isFading)
            StartCoroutine(_Fade(fadeIn, STANDARD_FADE_DURATION));
    }

    #region Scene Switching
    public void RestartScene(float duration = STANDARD_FADE_DURATION)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut += () => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void GoToScene(string sceneName)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut += () => LoadAsync(sceneName);
        }
    }

    public void GoToScene(int index)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut += () => LoadAsync(index);
        }
    }

    public void GoToScene(Scene scene)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut += () => LoadAsync(scene.name);
        }
    }

    public void Quit(float duration = STANDARD_FADE_DURATION)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut += () => Application.Quit();
        }
    }
    #endregion Scene Switching

    private void LoadAsync(int index)
    {
        loadOperation = SceneManager.LoadSceneAsync(index);
        loadOperation.allowSceneActivation = true;
    }

    private void LoadAsync(string sceneName)
    {
        loadOperation = SceneManager.LoadSceneAsync(sceneName);
        loadOperation.allowSceneActivation = true;
    }

    private IEnumerator _Fade(bool fadeIn, float duration)
    {
        isFading = true;

        var progress = 0f;
        var startColor = _fadeGraphic.color;
        var endColor = color;
        endColor.a = (fadeIn) ? 0f : 1f;

        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            _fadeGraphic.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        isFading = false;
        isFadedIn = fadeIn;

        if (fadeIn)
            OnFadeIn.Invoke();
        else
            OnFadeOut.Invoke();
    }
}
