﻿using Sirenix.Utilities;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private Chain[] chains;
    private Key[] keys;
    private Gate[] gates;
    private Hostile[] hostiles;

    private void Awake()
    {
        Util.GameManager = this;

        keys = FindObjectsOfType<Key>();
        gates = FindObjectsOfType<Gate>();
        chains = FindObjectsOfType<Chain>();
        hostiles = FindObjectsOfType<Hostile>();

        foreach (var key in keys)
        {
            key.Initialize(gates.First(gate => gate.Type == key.Type));
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Util.FadeManager.RestartScene();
        }
    }

    public void ResetStage()
    {
        if (chains.Any()) chains.ForEach(chain => chain.Reset());
        if (keys.Any()) keys.ForEach(key => key.Reset());
        if (gates.Any()) gates.ForEach(gate => gate.Reset());
        if (hostiles.Any()) hostiles.ForEach(hostile => hostile.Reset());
    }
}
