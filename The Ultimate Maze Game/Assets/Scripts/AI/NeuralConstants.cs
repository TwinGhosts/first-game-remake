using System.Collections.Generic;

public static class NeuralConstants
{
    public const int INPUT_NODE_COUNT = 11;
    public static readonly List<int> HIDDEN_NODE_COUNTS = new() { 5, 8, 5 };
    public const int OUPUT_NODE_COUNT = 2;
}