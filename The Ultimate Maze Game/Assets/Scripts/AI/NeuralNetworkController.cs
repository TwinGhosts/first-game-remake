using UnityEngine;
using System.Linq;
using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class NeuralNetworkController : MonoBehaviour
{
    [Header("Save / Loading")]
    [SerializeField] private bool loadAgents = false;

    [Header("Agents")]
    [Range(1, 500), SerializeField] private int agentCount = 10;
    [SerializeField] private float mutationRate = 0.08f;
    [SerializeField] private float rapidMutationRate = 0.33f;
    [SerializeField] private AINeural agentPrefab;

    [SerializeField] private NeuralNetwork[] agents;

    [SerializeField] private List<NeuralNetwork> allTimeBestAgents = new();
    [SerializeField] private List<NeuralNetwork> bestOfGenerationAgents = new();

    [SerializeField] private int generationBestAgentsMax = 5;
    [SerializeField] private int allTimeBestAgentsMax = 5;
    [SerializeField] private int agentSaveCount = 20;

    [Header("Generation")]
    [SerializeField] private int generation = 0;
    [SerializeField] private float generationTimeCap = 10f;

    private Transform startingTransform;
    private GameManager gameManager;
    private float currentTimeCap = 0f;
    private float firstUpdateDelay = 0.5f;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        startingTransform = GameObject.FindGameObjectWithTag("Start").transform;

        agents = new NeuralNetwork[agentCount];

        // Fix some values when not put in correctly via the editor
        if (generationBestAgentsMax > agentCount)
            generationBestAgentsMax = agentCount;

        if (allTimeBestAgentsMax > agentCount)
            allTimeBestAgentsMax = agentCount;
    }

    private void Start()
    {
        CreateNewGeneration(loadAgents);
    }

    private void Update()
    {
        currentTimeCap += Time.deltaTime;

        if (firstUpdateDelay > 0)
        {
            firstUpdateDelay -= Time.deltaTime;
            return;
        }

        if (currentTimeCap > generationTimeCap)
        {
            KillGeneration();
            currentTimeCap = 0;
        }

        // When there is no active simulation running, reset with a next generation
        if (!agents.Any(x => x.Actor.IsAlive))
        {
            CreateNewGeneticGeneration();
        }
    }

    [Button("Save Agents", ButtonHeight = 64)]
    public void SaveAgents()
    {
        NeuralSaver.SaveNetworksToResources(agents.ToList());
    }

    /// <summary>
    /// Creates a completely new generation of agents at the starting point
    /// </summary>
    private void CreateNewGeneration(bool loadAgents)
    {
        if (loadAgents)
        {
            var networks = NeuralSaver.LoadNetworksFromResources();
            networks = networks.OrderByDescending(x => x.Fitness).ToList();
            agents = networks.Take(Mathf.Min(agentCount, networks.Count)).ToArray();
        }
        else
        {
            agents = new NeuralNetwork[agentCount];
        }

        // Fix some values when not put in correctly via the editor
        if (generationBestAgentsMax > agentCount) generationBestAgentsMax = agentCount;
        if (allTimeBestAgentsMax > agentCount) allTimeBestAgentsMax = agentCount;

        // Create agents at the starting position
        var startPosition = startingTransform.position;
        startPosition.z = 0;
        for (var i = 0; i < agentCount; i++)
        {
            var agent = Instantiate(agentPrefab, startPosition, startingTransform.rotation);

            if (loadAgents)
            {
                agent.SetNeuralNetwork(agents[i]);
                agent.NeuralNetwork.SetActor(agent);
            }
            else
            {
                agent.InitializeNeuralNetwork();
            }

            agents[i] = agent.NeuralNetwork;
        }

        generation++;
    }

    /// <summary>
    /// Creates a new generation based on the previous best ones and the all-time best, the rest stays random
    /// </summary>
    private void CreateNewGeneticGeneration()
    {
        currentTimeCap = 0;

        var startPosition = startingTransform.position;
        startPosition.z = 0;

        var firstFraction = 0.33f;
        var secondFraction = 0.27f;
        var thirdFraction = 0.25f;

        if (generation > 0)
        {
            SetAllTimeBestNetworks();
            bestOfGenerationAgents = ReturnBestOfGeneration(generationBestAgentsMax).ToList();
        }

        var bestGenerationCopy = (NeuralNetwork[])allTimeBestAgents.ToArray().Clone();
        CreateAgents(0, agents.Length * firstFraction, bestGenerationCopy, "All time best");

        bestGenerationCopy = (NeuralNetwork[])bestOfGenerationAgents.ToArray().Clone();

        CreateAgents(agents.Length * firstFraction, agents.Length * (firstFraction + secondFraction), bestGenerationCopy, "Previous gen best");
        CreateAgents(agents.Length * (firstFraction + secondFraction), agents.Length * (firstFraction + secondFraction + thirdFraction), bestGenerationCopy, "Best heavily mutated", true);
        CreateRandomAgents(agents.Length * (firstFraction + secondFraction + thirdFraction), agents.Length);

        generation++;
    }

    private void CreateAgents(float start, float end, NeuralNetwork[] bestGenerationCopy, string namePrefix, bool heavilyMutated = false)
    {
        for (int i = (int)start; i < end; i++)
        {
            var agent = Instantiate(agentPrefab, startingTransform.position, startingTransform.rotation);
            agent.InitializeNeuralNetwork();
            agent.name = $"Agent {i}: {namePrefix}";
            agent.NeuralNetwork.Evolve(bestGenerationCopy[(int)Mathf.Repeat(i, bestGenerationCopy.Length)], bestGenerationCopy[(int)Mathf.Repeat(i + 1, bestGenerationCopy.Length)], heavilyMutated);
            Destroy(agents[i].Actor.gameObject);
            agents[i] = agent.NeuralNetwork;
        }
    }

    private void CreateRandomAgents(float start, float end)
    {
        for (int i = (int)start; i < end; i++)
        {
            var agent = Instantiate(agentPrefab, startingTransform.position, startingTransform.rotation);
            agent.InitializeNeuralNetwork();
            agent.name = $"Agent {i}: Random";
            Destroy(agents[i].Actor.gameObject);
            agents[i] = agent.NeuralNetwork;
        }
    }

    /// <summary>
    /// Returns an array with a number of the best performing networks in the current generation
    /// </summary>
    /// <returns>Best performing agents</returns>
    private NeuralNetwork[] ReturnBestOfGeneration(int amount)
    {
        // Make sure to keep the value between possible bounds
        amount = Mathf.Clamp(amount, 0, agentCount);

        // Copy the agents to keep the references in tact
        var bestPerformingAgents = agents;

        // Sort the array based on the fitness
        Array.Sort(bestPerformingAgents,
            delegate (NeuralNetwork x, NeuralNetwork y) { return y.Fitness.CompareTo(x.Fitness); });

        // Copy only the entries we want as deep copies later on
        var returnArray = new NeuralNetwork[amount];

        // Replace the current networks with a 'mixed' copy of the agents
        for (var i = 0; i < returnArray.Length; i++)
        {
            returnArray[i] = new NeuralNetwork(bestPerformingAgents[i]);
        }

        return returnArray;
    }

    /// <summary>
    /// Loops through all of the current all time best combined with all agents of this generation 
    /// and sorts them in descending order of fitness and sets the all time best to the top 'amount' of networks
    /// </summary>
    private void SetAllTimeBestNetworks()
    {
        // Create a mixed array of the agents and all time best
        var mixedArray = new NeuralNetwork[agents.Length + allTimeBestAgents.Count];
        agents.CopyTo(mixedArray, 0);
        allTimeBestAgents.CopyTo(mixedArray, agents.Length);

        // Sort descending
        Array.Sort(mixedArray,
            delegate (NeuralNetwork x, NeuralNetwork y) { return y.Fitness.CompareTo(x.Fitness); });

        // Clean the current all time best and replace it with the new all time best
        allTimeBestAgents = new List<NeuralNetwork>();
        for (var i = 0; i < allTimeBestAgentsMax; i++)
        {
            allTimeBestAgents.Add(mixedArray[i]);
        }
    }

    [Button("Kill generation", 64)]
    private void KillGeneration()
    {
        foreach (var agent in agents)
        {
            if (agent.Actor.IsAlive)
            {
                agent.Actor.Kill();
            }
        }

        gameManager.ResetStage();
    }
}
