using Sirenix.OdinInspector;
using System;
using UnityEditor;
using UnityEngine;

public class AINeural : MonoBehaviour
{
    public NeuralNetwork NeuralNetwork { get; protected set; }
    public bool IsAlive { get; protected set; } = true;
    public float FinishTime { get; protected set; } = float.MaxValue;

    [Header("Reward / Penalties")]
    [SerializeField] protected float timeReward = 0.5f;
    [SerializeField] protected float flowReward = 50f;
    [SerializeField] protected float exitReward = 2000f;
    [SerializeField] protected float objectiveReward = 500f;
    [SerializeField] protected float deathReward = -2500f;
    [SerializeField] protected float timeoutReward = -1000f;

    [Header("Configurables")]
    [SerializeField] protected float timeout = 10f;
    [SerializeField] protected float timeoutVelocityDeadzone = 0.15f;
    [SerializeField] protected float minDistanceThreshold = 0.5f;

    [ShowInInspector, ReadOnly] private Guid id = Guid.NewGuid();

    protected float currentTimeout = 0f;
    protected float currentTime = 0f;
    protected int shortestDistanceToEnd = int.MaxValue;

    protected Rigidbody2D body;
    protected SpriteRenderer spriteRenderer;
    protected AISensor sensorCollection;
    protected MovementController movementController;
    protected PathfindingVisualizer pathfindingVisualizer;

    protected void Awake()
    {
        sensorCollection = GetComponentInChildren<AISensor>();
        body = GetComponentInChildren<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        movementController = GetComponentInChildren<MovementController>();
        pathfindingVisualizer = FindObjectOfType<PathfindingVisualizer>();
    }

    private void Update()
    {
        if (body.velocity.magnitude < timeoutVelocityDeadzone)
        {
            currentTimeout += Time.deltaTime;
            if (currentTimeout >= timeout)
            {
                GiveReward(timeoutReward);
                IsAlive = false;
            }
        }
        else
        {
            currentTimeout = 0f;
        }

        currentTime += Time.deltaTime;
        if (currentTime >= 0.1f)
        {
            GiveReward(timeReward);
            currentTime = 0f;
        }

        var positionInt = new Vector3Int(Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.y));
        pathfindingVisualizer.DistanceGrid.TryGetValue(positionInt, out var distance);

        if (shortestDistanceToEnd == int.MaxValue)
            shortestDistanceToEnd = distance;

        if (shortestDistanceToEnd > distance)
        {
            shortestDistanceToEnd = distance;
            var reward = flowReward * (1f - ((float)shortestDistanceToEnd / pathfindingVisualizer.LongestDistance));
            GiveReward(reward);
        }
    }

    private void FixedUpdate()
    {
        // TODO Calculate fitness
        // TODO Check whether this agent has finished or not

        if (!IsAlive)
        {
            DeadLogic();
            return;
        }

        var sensorOutputs = sensorCollection.SensorOutputs();
        var output = NeuralNetwork.Query(sensorOutputs.ToArray());
        var horizontalInput = output[0];
        var verticalInput = output[1];

        // Debug.Log($"{output[0]} | {output[1]}");

        movementController.Move(horizontalInput, verticalInput);
    }

    public void SetNeuralNetwork(NeuralNetwork neuralNetwork)
    {
        NeuralNetwork = neuralNetwork;
    }

    public void InitializeNeuralNetwork()
    {
        NeuralNetwork = new NeuralNetwork(sensorCollection.SensorCount());
        NeuralNetwork.SetActor(this);
    }

    public void Finish(float finishTime)
    {
        FinishTime = finishTime;
        GiveReward(exitReward);
        IsAlive = false;
    }

    public void Kill()
    {
        if (!IsAlive)
            return;

        IsAlive = false;
        GiveReward(deathReward);
    }

    private void GiveReward(float reward)
    {
        if (!IsAlive)
            return;

        if (NeuralNetwork == null)
        {
            Debug.LogWarning($"{id} - Neural Network is null");
            return;
        }

        NeuralNetwork.GiveReward(reward);
    }

    private void DeadLogic()
    {
        body.velocity = Vector2.zero;
        var newColor = Color.red;
        newColor.a = 0.25f;
        spriteRenderer.color = newColor;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!IsAlive) return;

        var hostile = collision.collider.CompareTag(Tags.Hostile);
        if (hostile)
        {
            IsAlive = false;
        }
    }

    private void OnDrawGizmos()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.black;
        style.fontSize = 20;
        style.fontStyle = FontStyle.Bold;

        var labelPosition = transform.position + Vector3.forward;
        Handles.Label(labelPosition, NeuralNetwork.Fitness.ToString(), style);
    }
}
