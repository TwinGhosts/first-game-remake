using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class NeuralSaver
{
    private static string GetFilePath(string sceneId) => Path.Combine(Application.dataPath, "Resources", $"{sceneId}_networks.json");

    public static void SaveNetworksToResources(List<NeuralNetwork> networks)
    {
        var sceneId = SceneManager.GetActiveScene().name;
        var networkDataList = new List<NeuralNetworkData>();

        foreach (var network in networks)
        {
            networkDataList.Add(new NeuralNetworkData(network));
        }

        var json = JsonConvert.SerializeObject(networkDataList, Formatting.Indented);
        var path = GetFilePath(sceneId);

        File.WriteAllText(path, json);

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
        Debug.Log($"Saved networks to: '{path}'");
    }

    public static List<NeuralNetwork> LoadNetworksFromResources()
    {
        var sceneId = SceneManager.GetActiveScene().name; // or use buildIndex
        var path = GetFilePath(sceneId);
        var networks = new List<NeuralNetwork>();

        if (File.Exists(path))
        {
            var json = File.ReadAllText(path);
            var dataList = JsonConvert.DeserializeObject<List<NeuralNetworkData>>(json);

            foreach (var data in dataList)
            {
                networks.Add(new NeuralNetwork(data));
            }
        }

        return networks;
    }
}
