using System;
using System.Collections.Generic;

[Serializable]
public class NeuralNetworkData
{
    public Guid Guid { get; set; }
    public float MutationRate { get; set; }
    public float RapidMutationRate { get; set; }
    public float Fitness { get; set; }
    public float[] Input { get; set; }
    public float[] Output { get; set; }
    public List<float[]> HiddenLayers { get; set; }
    public float[][][] Weights { get; set; }

    public NeuralNetworkData()
    {
    }

    public NeuralNetworkData(NeuralNetwork agent)
    {
        Guid = agent.Guid;
        MutationRate = agent.MutationRate;
        RapidMutationRate = agent.RapidMutationRate;
        Fitness = agent.Fitness;
        Weights = agent.Weights.ToArray();
        Input = agent.Input;
        Output = agent.Output;
        HiddenLayers = agent.HiddenLayers;
    }
}