using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Pathfinding : MonoBehaviour
{
    [SerializeField] private TileBase walkableTile;
    [SerializeField] private TileBase wallTile;

    public Tilemap Tilemap => tilemap;
    public TileBase WalkableTile => walkableTile;
    public TileBase WallTile => wallTile;
    public Dictionary<Vector3Int, Node> Grid => grid;

    private Tilemap tilemap;
    private Dictionary<Vector3Int, Node> grid = new();

    private void Awake()
    {
        tilemap = FindObjectOfType<Tilemap>();

        CreateGrid();
    }

    public List<Node> FindPath(Vector3 start, Vector3 end)
    {
        var startNode = grid[tilemap.WorldToCell(start)];
        var endNode = grid[tilemap.WorldToCell(end)];

        var openList = new List<Node> { startNode };
        var closedList = new HashSet<Node>();

        while (openList.Count > 0)
        {
            var currentNode = openList[0];
            for (var i = 1; i < openList.Count; i++)
            {
                if (openList[i].FCost < currentNode.FCost || openList[i].FCost == currentNode.FCost && openList[i].HCost < currentNode.HCost)
                {
                    currentNode = openList[i];
                }
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (currentNode == endNode)
            {
                return RetracePath(startNode, endNode);
            }

            foreach (var neighbor in GetNeighbors(currentNode))
            {
                if (!neighbor.IsWalkable || closedList.Contains(neighbor))
                {
                    continue;
                }

                var newMovementCostToNeighbor = currentNode.GCost + GetDistance(currentNode, neighbor);
                if (newMovementCostToNeighbor < neighbor.GCost || !openList.Contains(neighbor))
                {
                    neighbor.GCost = newMovementCostToNeighbor;
                    neighbor.HCost = GetDistance(neighbor, endNode);
                    neighbor.Parent = currentNode;

                    if (!openList.Contains(neighbor))
                    {
                        openList.Add(neighbor);
                    }
                }
            }
        }

        return null; // Path not found
    }

    private void CreateGrid()
    {
        foreach (var pos in tilemap.cellBounds.allPositionsWithin)
        {
            var tile = tilemap.GetTile(pos);
            var isWalkable = tile == walkableTile;
            grid[pos] = new Node(pos, isWalkable);
        }
    }

    private List<Node> GetNeighbors(Node node)
    {
        var neighbors = new List<Node>();

        Vector3Int[] directions = {
            new(1, 0, 0),
            new(-1, 0, 0),
            new(0, 1, 0),
            new(0, -1, 0)
        };

        foreach (var direction in directions)
        {
            var neighborPos = node.Position + direction;
            if (grid.ContainsKey(neighborPos))
            {
                neighbors.Add(grid[neighborPos]);
            }
        }

        return neighbors;
    }

    private int GetDistance(Node a, Node b)
    {
        var dstX = Mathf.Abs(a.Position.x - b.Position.x);
        var dstY = Mathf.Abs(a.Position.y - b.Position.y);
        return dstX + dstY;
    }

    private List<Node> RetracePath(Node startNode, Node endNode)
    {
        var path = new List<Node>();
        var currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.Parent;
        }

        path.Reverse();
        return path;
    }
}
