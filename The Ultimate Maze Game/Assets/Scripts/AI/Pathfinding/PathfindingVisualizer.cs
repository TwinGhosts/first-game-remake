using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PathfindingVisualizer : MonoBehaviour
{
    [SerializeField] private Pathfinding pathfinder;
    [SerializeField] private Gradient pathDistanceGradient;

    public Dictionary<Vector3Int, int> DistanceGrid => distanceGrid;
    public Tilemap Tilemap => pathfinder.Tilemap;
    public int LongestDistance => longestDistance;
    public Pathfinding Pathfinder => pathfinder;
    public Dictionary<Vector3Int, List<Node>> ShortestPathGrid => shortestPathGrid;

    private Dictionary<Vector3Int, int> distanceGrid = new();
    private Dictionary<Vector3Int, List<Node>> shortestPathGrid = new();
    private int longestDistance = int.MaxValue;
    private Transform endTile;

    private void Awake()
    {
        endTile = GameObject.FindGameObjectWithTag("End").transform;
    }

    private void Start()
    {
        PopulateGrid();
    }

    public void PopulateGrid()
    {
        var tilemap = pathfinder.Tilemap;
        foreach (var position in tilemap.cellBounds.allPositionsWithin)
        {
            var path = pathfinder.FindPath(position, endTile.position);
            if (path != null)
            {
                distanceGrid.Add(position, path.Count);
                shortestPathGrid.Add(position, path);
            }
        }

        longestDistance = distanceGrid.Values.Max();
    }

    private void OnDrawGizmos()
    {
        if (!EditorApplication.isPlaying)
            return;

        var grid = pathfinder.Grid;
        var tilemap = pathfinder.Tilemap;

        if (distanceGrid == null)
            return;

        foreach (var node in grid.Values)
        {
            distanceGrid.TryGetValue(node.Position, out var distanceValue);

            if (node.IsWalkable)
            {
                var walkableColor = pathDistanceGradient.Evaluate(distanceValue / (float)longestDistance);
                walkableColor.a = 0.5f;

                Gizmos.color = walkableColor;
                Gizmos.DrawCube(tilemap.CellToWorld(node.Position) + tilemap.cellSize / 2, tilemap.cellSize * 0.9f);

                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.black;
                style.fontSize = 20;

                var labelPosition = tilemap.CellToWorld(node.Position) + tilemap.cellSize / 2;
                Handles.Label(labelPosition, distanceValue.ToString(), style);
            }
        }
    }
}