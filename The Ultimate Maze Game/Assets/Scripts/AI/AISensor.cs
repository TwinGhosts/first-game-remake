using System.Collections.Generic;
using UnityEngine;

public class AISensor : MonoBehaviour
{
    public WallSensor[] WallSensors => wallSensors;
    public FlowSensor FlowSensor => flowSensor;
    public DirectionSensor DirectionSensor => directionSensor;
    public ClosestDangerSensor DangerSensor => dangerSensor;
    public ObjectiveSensor ObjectiveSensor => objectiveSensor;
    public HostileSensor HostileSensor => hostileSensor;

    private WallSensor[] wallSensors;
    private FlowSensor flowSensor;
    private ClosestDangerSensor dangerSensor;
    private HostileSensor hostileSensor;
    private ObjectiveSensor objectiveSensor;
    private DirectionSensor directionSensor;

    private Rigidbody2D body;
    private AINeural actor;

    private void Awake()
    {
        actor = GetComponent<AINeural>();
        body = GetComponent<Rigidbody2D>();

        wallSensors = GetComponentsInChildren<WallSensor>();
        flowSensor = GetComponentInChildren<FlowSensor>(false);
        directionSensor = GetComponentInChildren<DirectionSensor>(false);
        dangerSensor = GetComponentInChildren<ClosestDangerSensor>(false);
        hostileSensor = GetComponentInChildren<HostileSensor>(false);
        objectiveSensor = GetComponentInChildren<ObjectiveSensor>(false);
    }

    private void Update()
    {
        UpdateSensors();
    }

    public int SensorCount()
    {
        var count = 0;

        foreach (var wallSensor in wallSensors)
        {
            count++;
        }

        if (flowSensor) count++;
        if (directionSensor) count++;
        if (dangerSensor) count++;
        if (hostileSensor) count++;
        if (objectiveSensor) count++;

        return count;
    }

    public List<float> SensorOutputs()
    {
        var returnList = new List<float>();

        foreach (var wallSensor in wallSensors)
        {
            returnList.Add(wallSensor.GetValue());
        }

        if (flowSensor) returnList.Add(flowSensor.GetValue());
        if (directionSensor) returnList.Add(directionSensor.GetValue());
        if (dangerSensor) returnList.Add(dangerSensor.GetValue());
        if (hostileSensor) returnList.Add(hostileSensor.GetValue());
        if (objectiveSensor) returnList.Add(objectiveSensor.GetValue());

        return returnList;
    }

    private void UpdateSensors()
    {
        foreach (var wallSensor in wallSensors)
        {
            wallSensor.Tick();
        }

        if (flowSensor) flowSensor.Tick();
        if (directionSensor) directionSensor.Tick();
        if (dangerSensor) dangerSensor.Tick();
        if (hostileSensor) hostileSensor.Tick();
        if (objectiveSensor) objectiveSensor.Tick();
    }
}