using UnityEngine;

public class FlowSensor : MonoBehaviour, ISensor
{
    [SerializeField] private Transform actor;

    private PathfindingVisualizer pathfindingVisualizer;

    private void Awake()
    {
        pathfindingVisualizer = FindObjectOfType<PathfindingVisualizer>();
    }

    public void Tick()
    {
        // Nothing
    }

    public float GetValue()
    {
        var actorPosition = actor.position;
        var tilemap = pathfindingVisualizer.Tilemap;
        var cellPosition = tilemap.WorldToCell(actorPosition);

        if (pathfindingVisualizer.DistanceGrid.TryGetValue(cellPosition, out var distance))
        {
            return distance / (float)pathfindingVisualizer.LongestDistance;
        }

        return 0f;
    }
}
