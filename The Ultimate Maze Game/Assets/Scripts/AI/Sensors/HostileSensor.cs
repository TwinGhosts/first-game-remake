using System.Linq;
using UnityEngine;

public class HostileSensor : MonoBehaviour, ISensor
{
    [SerializeField] private float detectionRadius = 2f;
    [SerializeField] private LayerMask hostileLayer;

    public float DetectionRadius => detectionRadius;

    private float dangerValue = 0f;

    public void Tick()
    {
        CheckForHostiles();
    }

    public float GetValue()
    {
        return Mathf.Clamp01(dangerValue);
    }

    private void CheckForHostiles()
    {
        var hostiles = Physics2D.OverlapCircleAll(transform.position, detectionRadius, hostileLayer);

        dangerValue = 0f;
        foreach (var hostile in hostiles)
        {
            Vector2 hostilePosition = hostile.transform.position;
            Vector2 agentPosition = transform.position;
            Vector2 hostileDirection = hostile.GetComponentInParent<Rigidbody2D>()?.velocity ?? Vector2.zero;

            var distance = Vector2.Distance(agentPosition, hostilePosition);
            var relativeDirection = (hostilePosition - agentPosition).normalized;
            var directionFactor = Vector2.Dot(relativeDirection, hostileDirection.normalized);

            var distanceValue = 1 - (distance / detectionRadius);
            var directionValue = (directionFactor + 1) / 2;

            var combinedValue = distanceValue * directionValue;
            dangerValue += combinedValue;
        }

        if (hostiles.Any())
        {
            dangerValue /= hostiles.Count();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }
}
