public interface ISensor
{
    float GetValue();

    void Tick();
}