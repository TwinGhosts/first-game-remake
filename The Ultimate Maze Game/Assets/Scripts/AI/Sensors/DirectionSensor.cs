using System.Collections.Generic;
using UnityEngine;

public class DirectionSensor : MonoBehaviour, ISensor
{
    [SerializeField] private Transform actor;

    private PathfindingVisualizer pathfindingVisualizer;
    private int currentDistance = int.MaxValue;
    private Vector3Int nextPosition;
    private List<Node> shortestPath;

    private void Awake()
    {
        pathfindingVisualizer = FindObjectOfType<PathfindingVisualizer>();
    }

    private void Start()
    {
        var tilemap = pathfindingVisualizer.Tilemap;
        var cellPosition = tilemap.WorldToCell(actor.position);

        currentDistance = pathfindingVisualizer.LongestDistance;
        shortestPath = pathfindingVisualizer.ShortestPathGrid[cellPosition];

        SetNextPosition();
    }

    public void Tick()
    {
        var tilemap = pathfindingVisualizer.Tilemap;
        var cellPosition = tilemap.WorldToCell(actor.position);

        if (nextPosition == cellPosition)
        {
            SetNextPosition();
        }
    }

    public float GetValue()
    {
        var tilemap = pathfindingVisualizer.Tilemap;
        var cellPosition = tilemap.WorldToCell(actor.position);

        if (pathfindingVisualizer.DistanceGrid.TryGetValue(cellPosition, out var distance))
        {
            // Normalize the distance value between 0 and 1
            var normalizedDistance = 1f - (distance / (float)pathfindingVisualizer.LongestDistance);
            return normalizedDistance;
        }

        // Return a value of 0 if the position is not walkable or outside the grid
        return 0f;
    }

    private void SetNextPosition()
    {
        var tilemap = pathfindingVisualizer.Tilemap;
        var cellPosition = tilemap.WorldToCell(actor.position);

        var index = shortestPath.FindIndex(node => node.Position == cellPosition);
        if (index < shortestPath.Count - 1)
        {
            nextPosition = shortestPath[index + 1].Position;
        }
    }
}
