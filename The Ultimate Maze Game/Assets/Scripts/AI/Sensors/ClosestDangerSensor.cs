using UnityEngine;

public class ClosestDangerSensor : MonoBehaviour, ISensor
{
    [SerializeField] private float detectionRadius = 2f;
    [SerializeField] private LayerMask hostileLayer;

    public float DetectionRadius => detectionRadius;

    private float closestHostileDistance = Mathf.Infinity;

    public void Tick()
    {
        CheckForHostiles();
    }

    public float GetValue()
    {
        if (closestHostileDistance == Mathf.Infinity)
        {
            return 0f; // No hostile detected within sensor range
        }
        else
        {
            // Debug.Log($"Close Danger Sensor: '{Mathf.Clamp01(1 - (closestHostileDistance / detectionRadius))}'");
            return Mathf.Clamp01(1 - (closestHostileDistance / detectionRadius));
        }
    }

    private void CheckForHostiles()
    {
        closestHostileDistance = Mathf.Infinity;
        var hostiles = Physics2D.OverlapCircleAll(transform.position, detectionRadius, hostileLayer);

        // Debug.Log($"Checking: {hostiles}");
        foreach (var hostile in hostiles)
        {
            float distance = Vector2.Distance(transform.position, hostile.transform.position);
            if (distance < closestHostileDistance)
            {
                closestHostileDistance = distance;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }
}
