using UnityEngine;

public class WallSensor : MonoBehaviour, ISensor
{
    [SerializeField] private Vector2 direction = Vector2.up;
    [SerializeField] private float length = 2f;

    public float HitDistance => hitDistance;
    public RaycastHit2D Hit => hit;
    public Vector2 Direction => direction;
    public float Length => length;

    private RaycastHit2D hit;
    private float hitDistance = Mathf.Infinity;

    private const string MASK_NAME = "Wall";

    public void Tick()
    {
        var position = (Vector2)transform.position;

        CheckForWall();

        // When a wall has been found, draw the line to raycast hit point
        if (hit)
        {
            Debug.DrawLine(position, hit.point, Color.cyan);
            hitDistance = (hit.point - position).magnitude;
        }
        // When no wall has been found, draw a line in the sensor's direction and the max length
        else
            Debug.DrawLine(position, position + direction * length, Color.cyan);
    }

    public float GetValue()
    {
        // Normalize the hit distance
        if (hitDistance == Mathf.Infinity)
        {
            return 1f; // No wall detected within sensor range
        }
        else
        {
            return Mathf.Clamp01(1 - (hitDistance / length));
        }
    }

    private void CheckForWall()
    {
        hit = Physics2D.Raycast(transform.position, direction, length, 1 << LayerMask.NameToLayer(MASK_NAME), -100f, 100f);
    }
}