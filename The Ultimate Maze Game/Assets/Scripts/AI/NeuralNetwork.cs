using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class NeuralNetwork
{
    [SerializeField] private float mutationRate = 0.08f;
    [SerializeField] private float rapidMutationRate = 0.33f;
    [SerializeField] private float fitness = 0;
    [SerializeField] private AINeural actor = null;

    public Guid Guid { get; private set; } = Guid.NewGuid();
    public float Fitness => fitness;
    public float MutationRate => mutationRate;
    public AINeural Actor => actor;
    public float RapidMutationRate => rapidMutationRate;
    public float[] Input => input;
    public float[] Output => output;
    public List<float[]> HiddenLayers => hiddenLayers;
    public List<float[][]> Weights => weights;

    private float[] input;
    private float[] output;
    private List<float[]> hiddenLayers;

    private readonly List<float[][]> weights;

    private System.Random random;

    public NeuralNetwork(int inputNodes)
    {
        random = new System.Random(GetHashCode());

        var inputNodeCount = inputNodes;
        var hiddenNodeCounts = NeuralConstants.HIDDEN_NODE_COUNTS;
        var outputNodeCount = NeuralConstants.OUPUT_NODE_COUNT;

        input = new float[inputNodeCount];
        output = new float[outputNodeCount];
        hiddenLayers = new List<float[]>();

        // Initialize hidden layers
        foreach (var hiddenLayerSize in hiddenNodeCounts)
        {
            hiddenLayers.Add(new float[hiddenLayerSize]);
        }

        // Initialize weights
        weights = new List<float[][]>
        {
            // Input to first hidden layer
            InitializeWeights(hiddenNodeCounts[0], inputNodeCount)
        };

        // Between hidden layers
        for (var i = 1; i < hiddenNodeCounts.Count; i++)
        {
            weights.Add(InitializeWeights(hiddenNodeCounts[i], hiddenNodeCounts[i - 1]));
        }

        // Last hidden layer to output
        weights.Add(InitializeWeights(outputNodeCount, hiddenNodeCounts[hiddenNodeCounts.Count - 1]));
    }

    public NeuralNetwork(int inputNodes, List<int> hiddenNodes, int outputNodes)
    {
        random = new System.Random(GetHashCode());

        input = new float[inputNodes];
        output = new float[outputNodes];
        hiddenLayers = new List<float[]>();

        // Initialize hidden layers
        foreach (var nodes in hiddenNodes)
        {
            hiddenLayers.Add(new float[nodes]);
        }

        // Initialize weights
        weights = new List<float[][]>
        {
            // Input to first hidden layer
            InitializeWeights(hiddenNodes[0], inputNodes)
        };

        // Between hidden layers
        for (var i = 1; i < hiddenNodes.Count; i++)
        {
            weights.Add(InitializeWeights(hiddenNodes[i], hiddenNodes[i - 1]));
        }

        // Last hidden layer to output
        weights.Add(InitializeWeights(outputNodes, hiddenNodes[hiddenNodes.Count - 1]));
    }

    public NeuralNetwork(NeuralNetwork copyReference)
    {
        input = (float[])copyReference.input.Clone();
        output = (float[])copyReference.output.Clone();
        hiddenLayers = new List<float[]>();

        foreach (var layer in copyReference.hiddenLayers)
        {
            hiddenLayers.Add((float[])layer.Clone());
        }

        weights = new List<float[][]>();

        foreach (var weightMatrix in copyReference.weights)
        {
            weights.Add(DeepCloneArray(weightMatrix));
        }

        fitness = copyReference.fitness;

        random = new System.Random(GetHashCode());
    }

    public NeuralNetwork(NeuralNetworkData data)
    {
        Guid = data.Guid;
        input = data.Input;
        output = data.Output;
        hiddenLayers = data.HiddenLayers;
        weights = data.Weights.ToList();
        fitness = data.Fitness;
        mutationRate = data.MutationRate;
        rapidMutationRate = data.RapidMutationRate;
    }

    public void SetActor(AINeural actor)
    {
        this.actor = actor;
    }

    public float[] Query(float[] input)
    {
        this.input = input;

        // Input to first hidden layer
        hiddenLayers[0] = ComputeLayer(input, weights[0]);

        // Between hidden layers
        for (var i = 1; i < hiddenLayers.Count; i++)
        {
            hiddenLayers[i] = ComputeLayer(hiddenLayers[i - 1], weights[i]);
        }

        // Last hidden layer to output
        output = ComputeLayer(hiddenLayers[hiddenLayers.Count - 1], weights[weights.Count - 1]);

        return output;
    }

    public void Evolve(NeuralNetwork father, NeuralNetwork mother, bool rapidMutation = false)
    {
        var selectedParent = (random.Next(0, 100) >= 50) ? father : mother;
        for (var i = 0; i < weights.Count; i++)
        {
            MutateWeights(weights[i], selectedParent.weights[i], rapidMutation);
        }
    }

    private float[] ComputeLayer(float[] input, float[][] weights)
    {
        var tempOutput = new float[weights.Length];
        for (var y = 0; y < weights.Length; y++)
        {
            var sum = 0f;
            for (var x = 0; x < weights[y].Length; x++)
            {
                sum += input[x] * weights[y][x];
            }

            tempOutput[y] = (float)Math.Tanh(sum);
        }
        return tempOutput;
    }

    private float[][] InitializeWeights(int rows, int cols)
    {
        var tempWeights = new float[rows][];
        for (var i = 0; i < rows; i++)
        {
            tempWeights[i] = new float[cols];
            for (var j = 0; j < cols; j++)
            {
                tempWeights[i][j] = (float)random.NextDouble() * 2 - 1;
            }
        }
        return tempWeights;
    }

    private void MutateWeights(float[][] weights, float[][] parentWeights, bool rapidMutation = false)
    {
        for (var i = 0; i < weights.Length; i++)
        {
            for (var j = 0; j < weights[i].Length; j++)
            {
                weights[i][j] = parentWeights[i][j];
                if (random.NextDouble() < (rapidMutation ? rapidMutationRate : mutationRate))
                {
                    weights[i][j] += (float)(random.NextDouble() * 2 - 1) * 0.1f;
                }
            }
        }
    }

    private float[][] DeepCloneArray(float[][] array)
    {
        var newArray = new float[array.Length][];
        for (var i = 0; i < array.Length; i++)
        {
            newArray[i] = (float[])array[i].Clone();
        }
        return newArray;
    }

    public void GiveReward(float reward)
    {
        fitness += reward;
    }

    public void SetFitness(float value)
    {
        fitness = value;
    }
}
